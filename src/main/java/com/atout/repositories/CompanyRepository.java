package com.atout.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.atout.entities.Company;

@RepositoryRestResource
public interface CompanyRepository extends JpaRepository<Company, Long>{

}
